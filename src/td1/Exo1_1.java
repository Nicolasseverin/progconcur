package td1;

import java.util.Scanner;

public class Exo1_1 extends Thread {
	private String txt; private int nb; private int tmp;
	
	public Exo1_1(int nb, String txt, int tmp) {
		this.nb = nb; this.txt = txt; this.tmp = tmp;
	}
	
	public void run() {
		try {
			for (int i=0; i<this.nb; i++) {
				System.out.println(this.txt); sleep(this.tmp);
			}
		} catch (InterruptedException e) {System.out.println("ERREUR");}
	}
	
	public static void main(String[] args) {
		
		//Instanciation du scanner pour la saisie
		Scanner sc = new Scanner(System.in);
		
		//Demande et Saisie pour les differentes variables
		System.out.println("Veulliez saisir le nombre d'affichage n1 : ");
		int n1 = sc.nextInt();
		System.out.println("Veulliez saisir le texte txt1 : ");
		String txt1 = sc.next();
		System.out.println("Veulliez saisir le temps de repos t1 : ");
		int t1 = sc.nextInt();
		
		System.out.println("Veulliez saisir le nombre d'affichage n2 : ");
		int n2 = sc.nextInt();
		System.out.println("Veulliez saisir le texte txt2 : ");
		String txt2 = sc.next();
		System.out.println("Veulliez saisir le temps de repos t2 : ");
		int t2 = sc.nextInt();
		
		//Instanciation des threads
		Exo1_1 thread1 = new Exo1_1(n1,txt1,t1);
		Exo1_1 thread2 = new Exo1_1(n2,txt2,t2);
		
		//Demarrage des processus
		thread1.start(); thread2.start();
	}
}
